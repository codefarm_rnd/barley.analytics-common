/*
 * Copyright (c) 2005 - 2014, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package barley.event.publisher.core.internal.ds;

import java.io.File;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import barley.core.configuration.ServerConfiguration;
import barley.core.utils.BarleyUtils;
import barley.core.utils.ConfigurationContextService;
import barley.event.output.adapter.core.OutputEventAdapterFactory;
import barley.event.output.adapter.core.OutputEventAdapterService;
import barley.event.processor.manager.core.EventManagementService;
import barley.event.publisher.core.config.EventPublisherConstants;
import barley.event.publisher.core.exception.EventPublisherConfigurationException;
import barley.event.publisher.core.internal.CarbonEventPublisherManagementService;
import barley.event.publisher.core.internal.CarbonEventPublisherService;
import barley.event.publisher.core.internal.util.helper.EventPublisherConfigurationFilesystemInvoker;
import barley.event.stream.core.EventStreamService;
import barley.event.stream.core.internal.util.helper.EventStreamConfigurationFileSystemInvoker;
import barley.registry.core.exceptions.RegistryException;
import barley.registry.core.service.RegistryService;


/**
 * @scr.component name="eventPublisherService.component" immediate="true"
 * @scr.reference name="eventAdapter.service"
 * interface="org.wso2.carbon.event.output.adapter.core.OutputEventAdapterService" cardinality="1..1"
 * policy="dynamic" bind="setEventAdapterService" unbind="unsetEventAdapterService"
 * @scr.reference name="output.event.adapter.tracker.service"
 * interface="org.wso2.carbon.event.output.adapter.core.OutputEventAdapterFactory" cardinality="0..n"
 * policy="dynamic" bind="setEventAdapterType" unbind="unSetEventAdapterType"
 * @scr.reference name="eventManagement.service"
 * interface="org.wso2.carbon.event.processor.manager.core.EventManagementService" cardinality="1..1"
 * policy="dynamic" bind="setEventManagementService" unbind="unsetEventManagementService"
 * @scr.reference name="registry.service"
 * interface="org.wso2.carbon.registry.core.service.RegistryService"
 * cardinality="1..1" policy="dynamic" bind="setRegistryService" unbind="unsetRegistryService"
 * @scr.reference name="eventStreamManager.service"
 * interface="org.wso2.carbon.event.stream.core.EventStreamService" cardinality="1..1"
 * policy="dynamic" bind="setEventStreamService" unbind="unsetEventStreamService"
 * @scr.reference name="config.context.service"
 * interface="org.wso2.carbon.utils.ConfigurationContextService" cardinality="0..1" policy="dynamic"
 * bind="setConfigurationContextService" unbind="unsetConfigurationContextService"
 */
public class EventPublisherServiceDS {
    private static final Log log = LogFactory.getLog(EventPublisherServiceDS.class);

    public void activate() {
        try {
            checkIsStatsEnabled();
            CarbonEventPublisherService carbonEventPublisherService = new CarbonEventPublisherService();
            EventPublisherServiceValueHolder.registerPublisherService(carbonEventPublisherService);

            CarbonEventPublisherManagementService carbonEventPublisherManagementService = new CarbonEventPublisherManagementService();
            EventPublisherServiceValueHolder.getEventManagementService().subscribe(carbonEventPublisherManagementService);
            EventPublisherServiceValueHolder.registerPublisherManagementService(carbonEventPublisherManagementService);

            // (임시주석)
            //context.getBundleContext().registerService(EventPublisherService.class.getName(), carbonEventPublisherService, null);
            if (log.isDebugEnabled()) {
                log.debug("Successfully deployed EventPublisherService");
            }

            activateInactiveEventPublisherConfigurations(carbonEventPublisherService);
            
            // (추가) 2018.09.14 - 시작시 강제 config deploy 수행
            deployDefaultEventPublisherConfiguration();

            // (임시주석)
            //context.getBundleContext().registerService(EventStreamListener.class.getName(), new EventStreamListenerImpl(), null);

        } catch (RuntimeException e) {
            log.error("Could not create EventPublisherService : " + e.getMessage(), e);
        }
    }

    // (추가) 
    private void deployDefaultEventPublisherConfiguration() {
    	String mappingResourcePath = BarleyUtils.getCarbonConfigDirPath() + File.separator + EventPublisherConstants.EF_CONFIG_DIRECTORY; 
    	try {
    		File mappingResourceDir = new File(mappingResourcePath);
    		if(mappingResourceDir.exists()) {
    			for(File configFile : mappingResourceDir.listFiles()) {
    				EventPublisherConfigurationFilesystemInvoker.deploy(configFile.getAbsolutePath());
    			}
    		}
        } catch (EventPublisherConfigurationException e) {
            log.error(e.getMessage(), e);
        }
	}
	

    private void checkIsStatsEnabled() {
        ServerConfiguration config = ServerConfiguration.getInstance();
        String confStatisticsReporterDisabled = config.getFirstProperty("StatisticsReporterDisabled");
        if (!"".equals(confStatisticsReporterDisabled)) {
            boolean disabled = Boolean.valueOf(confStatisticsReporterDisabled);
            if (disabled) {
                return;
            }
        }
        EventPublisherServiceValueHolder.setGlobalStatisticsEnabled(true);
    }

    private void activateInactiveEventPublisherConfigurations(CarbonEventPublisherService carbonEventPublisherService) {
        Set<String> outputEventAdapterTypes = EventPublisherServiceValueHolder.getOutputEventAdapterTypes();
        outputEventAdapterTypes.addAll(EventPublisherServiceValueHolder.getOutputEventAdapterService().getOutputEventAdapterTypes());
        for (String type : outputEventAdapterTypes) {
            try {
                carbonEventPublisherService.activateInactiveEventPublisherConfigurationsForAdapter(type);
            } catch (EventPublisherConfigurationException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public void setEventAdapterService(
            OutputEventAdapterService outputEventAdapterService) {
        EventPublisherServiceValueHolder.registerEventAdapterService(outputEventAdapterService);
    }

    public void unsetEventAdapterService(
            OutputEventAdapterService outputEventAdapterService) {
        EventPublisherServiceValueHolder.getOutputEventAdapterTypes().clear();
        EventPublisherServiceValueHolder.registerEventAdapterService(null);
    }

    public void setRegistryService(RegistryService registryService) throws RegistryException {
        EventPublisherServiceValueHolder.setRegistryService(registryService);
    }

    public void unsetRegistryService(RegistryService registryService) {
        EventPublisherServiceValueHolder.unSetRegistryService();
    }

    public void setEventStreamService(EventStreamService eventStreamService) {
        EventPublisherServiceValueHolder.registerEventStreamService(eventStreamService);
    }

    public void unsetEventStreamService(EventStreamService eventStreamService) {
        EventPublisherServiceValueHolder.registerEventStreamService(null);
    }

    public void setEventAdapterType(OutputEventAdapterFactory outputEventAdapterFactory) {
        EventPublisherServiceValueHolder.addOutputEventAdapterType(outputEventAdapterFactory.getType());
        if (EventPublisherServiceValueHolder.getCarbonEventPublisherService() != null) {
            try {
                EventPublisherServiceValueHolder.getCarbonEventPublisherService().activateInactiveEventPublisherConfigurationsForAdapter(outputEventAdapterFactory.getType());
            } catch (EventPublisherConfigurationException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public void unSetEventAdapterType(OutputEventAdapterFactory outputEventAdapterFactory) {
        EventPublisherServiceValueHolder.removeOutputEventAdapterType(outputEventAdapterFactory.getType());
        if (EventPublisherServiceValueHolder.getCarbonEventPublisherService() != null) {
            try {
                EventPublisherServiceValueHolder.getCarbonEventPublisherService().deactivateActiveEventPublisherConfigurationsForAdapter(outputEventAdapterFactory.getType());
            } catch (EventPublisherConfigurationException e) {
                log.error(e.getMessage(), e);
            }
        }

    }

    protected void setConfigurationContextService(
            ConfigurationContextService configurationContextService) {
        EventPublisherServiceValueHolder.setConfigurationContextService(configurationContextService);
    }

    protected void unsetConfigurationContextService(
            ConfigurationContextService configurationContextService) {
        EventPublisherServiceValueHolder.setConfigurationContextService(null);

    }

    public void setEventManagementService(EventManagementService eventManagementService) {
        EventPublisherServiceValueHolder.registerEventManagementService(eventManagementService);

    }

    public void unsetEventManagementService(EventManagementService eventManagementService) {
        EventPublisherServiceValueHolder.registerEventManagementService(null);
        eventManagementService.unsubscribe(EventPublisherServiceValueHolder.getCarbonEventPublisherManagementService());

    }


}
