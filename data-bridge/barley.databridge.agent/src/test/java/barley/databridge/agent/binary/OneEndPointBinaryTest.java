package barley.databridge.agent.binary;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import barley.core.MultitenantConstants;
import barley.core.ServerConstants;
import barley.core.context.PrivilegedBarleyContext;
import barley.core.internal.BarleyContextDataHolder;
import barley.core.internal.OSGiDataHolder;
import barley.core.utils.BarleyUtils;
import barley.databridge.agent.AgentHolder;
import barley.databridge.agent.DataPublisher;
import barley.databridge.agent.DataPublisherTestUtil;
import barley.databridge.agent.exception.DataEndpointAgentConfigurationException;
import barley.databridge.agent.exception.DataEndpointAuthenticationException;
import barley.databridge.agent.exception.DataEndpointConfigurationException;
import barley.databridge.agent.exception.DataEndpointException;
import barley.databridge.commons.Event;
import barley.databridge.commons.exception.TransportException;
import barley.databridge.commons.utils.DataBridgeCommonsUtils;
import barley.databridge.core.DataBridgeServiceValueHolder;
import barley.registry.core.config.RegistryContext;
import barley.registry.core.jdbc.realm.InMemoryRealmService;
import barley.registry.core.service.RegistryService;
import barley.user.core.service.RealmService;

public class OneEndPointBinaryTest {
	
	// stream-definitions.xml 에 있는 스트림 설정의 name, version이 동일해야 함.
	private static final String STREAM_NAME = "barley.throttle.request.stream";
    private static final String VERSION = "1.1.0";
    private String agentConfigFileName = "data-agent-config.xml";
    private BinaryTestServer testServer = null;
    
    private static RealmService realmService = null;
	private static RegistryService registryService = null;
	
	/*
	 * (https://docs.wso2.com/display/DAS300/Understanding+Event+Streams+and+Event+Tables 참조)
	   Meta Data: Contains the meta information of the events. (Referred to as meta_<attribute name>.)
	   Correlation Data: Contains the correlation information of the events. (Referred to as correlation_<attribute name>.)
       Payload Data: Contains the actual data that the event intends to have. (Referred to as <attribute name>.)
	*/
	/*
	private static final String STREAM_DEFN = "{" +
            "  'name':'" + STREAM_NAME + "'," +
            "  'version':'" + VERSION + "'," +
            "  'nickName': 'Stock Quote Information'," +
            "  'description': 'Some Desc'," +
            "  'tags':['foo', 'bar']," +
            "  'metaData':[" +
            "          {'name':'ipAdd','type':'STRING'}" +
            "  ]," +
            "  'payloadData':[" +
            "          {'name':'symbol','type':'STRING'}," +
            "          {'name':'price','type':'DOUBLE'}," +
            "          {'name':'volume','type':'INT'}," +
            "          {'name':'max','type':'DOUBLE'}," +
            "          {'name':'min','type':'Double'}" +
            "  ]" +
            "}";
            */
	

	@BeforeClass
	public static void init() {
		initBarleyProperty();
//		initKeyStore();
		initTrustStore();
//		initBaseService();
//		initTenantDomain();
//		initDataHolder();
	}
	
	private static void initBarleyProperty() {
		String rootPath = "D:\\Workspace_STS_SaaSPlatform\\Workspace_STS_Analytics\\barley.analytics-common\\data-bridge\\barley.databridge.agent";
		String configPath = rootPath + File.separator + "src\\test\\resources\\" + "repository" + File.separator + "conf";
		System.setProperty(ServerConstants.CARBON_HOME, rootPath);
        System.setProperty(ServerConstants.CARBON_CONFIG_DIR_PATH, configPath);
        System.setProperty("carbon.registry.character.encoding", "UTF-8");
        
        // The line below is responsible for initializing the cache.
        BarleyContextDataHolder.getCurrentCarbonContextHolder();
	}
	
	private static void initKeyStore() {
		String resourcesPath = BarleyUtils.getCarbonHome() + File.separator + "src\\test\\resources\\repository\\resources";
		System.setProperty("Security.KeyStore.Location", resourcesPath + File.separator + "security" + File.separator + "wso2carbon.jks");
		System.setProperty("Security.KeyStore.Password", "wso2carbon");
	}
	
	private static void initTrustStore() {
		String resourcesPath = BarleyUtils.getCarbonHome() + File.separator + "src\\test\\resources\\repository\\resources";
		System.setProperty("javax.net.ssl.trustStore", resourcesPath + File.separator + "security" + File.separator + "client-truststore.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "wso2carbon");
	}
	
	private static void initTenantDomain() {
//		String tenantDomain = "codefarm.co.kr";
//		int tenantId = 1;
		String tenantDomain = MultitenantConstants.SUPER_TENANT_DOMAIN;
		int tenantId = MultitenantConstants.SUPER_TENANT_ID;
		PrivilegedBarleyContext.getThreadLocalCarbonContext().setTenantDomain(tenantDomain, true);
    	PrivilegedBarleyContext.getThreadLocalCarbonContext().setTenantId(tenantId);
	}
	
	private static void initBaseService() {
		try {
        	BasicDataSource dataSource = new BasicDataSource();
        	String connectionUrl = "jdbc:mysql://172.16.2.201:3306/barley_registry";
            dataSource.setUrl(connectionUrl);
            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
            dataSource.setUsername("cdfcloud");
            dataSource.setPassword("cdfcloud");
        	
            realmService = new InMemoryRealmService(dataSource);
            InputStream is = new FileInputStream(BarleyUtils.getCarbonConfigDirPath() + File.separator + "registry.xml");
            // registry.xml 정보와 DataSource가 가미된 realmService를 인자로 주어 RegistryContext를 생성한다. 
            RegistryContext ctx = RegistryContext.getBaseInstance(is, realmService);
            
            ctx.setSetup(true);
            ctx.selectDBConfig("mysql-db");
            
            realmService = ctx.getRealmService();
            registryService = ctx.getEmbeddedRegistryService();
        } catch (Exception e) {
        	e.printStackTrace();
        }
	}
	
	private static void initDataHolder() {
		OSGiDataHolder.getInstance().setUserRealmService(realmService);
		DataBridgeServiceValueHolder.setRealmService(realmService);
	}
	
	private synchronized void startServer() throws Exception {
		testServer = new BinaryTestServer();
		testServer.setUp(realmService);
		testServer.start();
	}
	
	@Test
	public void testOneDataEndpoint() throws Exception {
        startServer();
        AgentHolder.setConfigPath(DataPublisherTestUtil.getDataAgentConfigPath(agentConfigFileName));
        String hostName = DataPublisherTestUtil.LOCAL_HOST;
//        DataPublisher dataPublisher = new DataPublisher("Binary", "tcp://" + hostName + ":9611",
//                "ssl://" + hostName + ":9711", "admin@codefarm.co.kr", "admin");
        DataPublisher dataPublisher = new DataPublisher("Binary", "tcp://" + hostName + ":9611",
                "ssl://" + hostName + ":9711", "wso2.system.user@carbon.super", "admin");
        Event event = new Event();
        event.setStreamId(DataBridgeCommonsUtils.generateStreamId(STREAM_NAME, VERSION));
        event.setMetaData(new Object[]{"127.0.0.1"});
        event.setCorrelationData(new Object[]{100});
        event.setPayloadData(new Object[]{"Google API Test", 180, 2});
        
        int numberOfEventsSent = 1;
        for (int i = 0; i < numberOfEventsSent; i++) {
            dataPublisher.publish(event);
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }
        dataPublisher.shutdown();
        Assert.assertEquals(numberOfEventsSent, testServer.getNumberOfEventsReceived());
        testServer.resetReceivedEvents();
        testServer.stop();
    }
	
	// api.throttle.request.stream_1.0.0.json, stream-definitions.xml 설정 모두 동일해야 함. 
	// 설정의 name, version이 동일해야 함.
	@Test
	public void testPublishData() throws Exception {
		// 서버는 따로 구동시킨 후 데이터만 전달 
		AgentHolder.setConfigPath(DataPublisherTestUtil.getDataAgentConfigPath(agentConfigFileName));
        String hostName = DataPublisherTestUtil.LOCAL_HOST;
//        DataPublisher dataPublisher = new DataPublisher("Binary", "tcp://" + hostName + ":9611",
//                "ssl://" + hostName + ":9711", "admin@codefarm.co.kr", "admin");
        DataPublisher dataPublisher = new DataPublisher("Binary", "tcp://" + hostName + ":9611",
                "ssl://" + hostName + ":9711", "wso2.system.user@carbon.super", "admin");
        
        Event event = new Event();
        event.setStreamId(DataBridgeCommonsUtils.generateStreamId(STREAM_NAME, VERSION));
        event.setMetaData(new Object[]{"127.0.0.1"});
        event.setCorrelationData(new Object[]{"1000"});
        event.setPayloadData(new Object[]{"Google API Test", 180.0, 2});
        
//        Event event = new Event(DataBridgeCommonsUtils.generateStreamId(STREAM_NAME, VERSION), 
//        		System.currentTimeMillis(), new Object[]{"127.0.0.1"}, new Object[]{100}, new Object[]{"Google API Test", 180, 2});
        
        // data-agent-config.xml의 전략을 sync로 동작. async일 경우는 스킵하자.    
        dataPublisher.publish(event);
        /*try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }*/
        dataPublisher.shutdownWithAgent();
	}
	
	
	
}

