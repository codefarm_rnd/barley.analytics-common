/*
 * Copyright 2004,2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package barley.databridge.core.internal;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import org.osgi.framework.ServiceRegistration;
//import org.osgi.service.component.ComponentContext;

import barley.core.MultitenantConstants;
import barley.core.context.PrivilegedBarleyContext;
import barley.core.utils.ConfigurationContextService;
import barley.databridge.commons.StreamDefinition;
import barley.databridge.commons.exception.DifferentStreamDefinitionAlreadyDefinedException;
import barley.databridge.commons.exception.MalformedStreamDefinitionException;
import barley.databridge.commons.utils.EventDefinitionConverterUtils;
import barley.databridge.core.DataBridge;
import barley.databridge.core.DataBridgeReceiverService;
import barley.databridge.core.DataBridgeServiceValueHolder;
import barley.databridge.core.DataBridgeSubscriberService;
import barley.databridge.core.definitionstore.AbstractStreamDefinitionStore;
import barley.databridge.core.exception.StreamDefinitionStoreException;
import barley.databridge.core.internal.authentication.CarbonAuthenticationHandler;
import barley.databridge.core.internal.utils.DataBridgeCoreBuilder;
import barley.identity.authentication.AuthenticationService;
import barley.user.api.UserStoreException;
import barley.user.core.service.RealmService;

/**
 * @scr.component name="databridge.component" immediate="true"
 * @scr.reference name="org.wso2.carbon.identity.authentication.internal.AuthenticationServiceComponent"
 * interface="org.wso2.carbon.identity.authentication.AuthenticationService"
 * cardinality="1..1" policy="dynamic" bind="setAuthenticationService"  unbind="unsetAuthenticationService"
 * @scr.reference name="user.realmservice.default" interface="org.wso2.carbon.user.core.service.RealmService"
 * cardinality="1..1" policy="dynamic" bind="setRealmService"  unbind="unsetRealmService"
 * @scr.reference name="stream.definitionStore.service"
 * interface="org.wso2.carbon.databridge.core.definitionstore.AbstractStreamDefinitionStore" cardinality="1..1"
 * policy="dynamic" bind="setEventStreamStoreService" unbind="unsetEventStreamStoreService"
 * @scr.reference name="config.context.service"
 * interface="org.wso2.carbon.utils.ConfigurationContextService" cardinality="0..1"
 * policy="dynamic"  bind="setConfigurationContextService" unbind="unsetConfigurationContextService"
 */
public class DataBridgeDS {
    private static final Log log = LogFactory.getLog(DataBridgeDS.class);
    private AuthenticationService authenticationService;
    //private ServiceRegistration receiverServiceRegistration;
    //private ServiceRegistration subscriberServiceRegistration;
    private DataBridge databridge;
    //private ServiceRegistration databridgeRegistration;

    /**
     * initialize the agent server here.
     *
     * @param context
     */
    public void activate() {
        try {
            if (databridge == null) {
                AbstractStreamDefinitionStore streamDefinitionStore = DataBridgeServiceValueHolder.getStreamDefinitionStore();
                databridge = new DataBridge(new CarbonAuthenticationHandler(authenticationService),
                        streamDefinitionStore, DataBridgeCoreBuilder.getDatabridgeConfigPath());
                try {
                	// data-bridge/stream-definitions.xml 읽어와 스트림 정보 저장 
                    List<String[]> streamDefinitionStrings = DataBridgeCoreBuilder.loadStreamDefinitionXML();
                    for (String[] streamDefinitionString : streamDefinitionStrings) {
                        try {
                            StreamDefinition streamDefinition = EventDefinitionConverterUtils.convertFromJson(streamDefinitionString[1]);
                            int tenantId = DataBridgeServiceValueHolder.getRealmService().getTenantManager().getTenantId(streamDefinitionString[0]);
                            if (tenantId == MultitenantConstants.INVALID_TENANT_ID) {
                                log.warn("Tenant " + streamDefinitionString[0] + " does not exist, Error in defining event stream " + streamDefinitionString[1]);
                                continue;
                            }

                            try {
                                PrivilegedBarleyContext.startTenantFlow();
                                PrivilegedBarleyContext privilegedCarbonContext = PrivilegedBarleyContext.getThreadLocalCarbonContext();
                                privilegedCarbonContext.setTenantId(tenantId);
                                privilegedCarbonContext.setTenantDomain(streamDefinitionString[0]);

                                // FileSystemStreamDefinitionStore.saveStreamDefinitionToStore() - CarbonEventStreamService.addEventStreamDefinition() 수행 
                                // eventstreams/*.json 읽어와 스트림 정보 저장 
                                streamDefinitionStore.saveStreamDefinition(streamDefinition, tenantId);

                            } catch (DifferentStreamDefinitionAlreadyDefinedException e) {
                                log.warn("Error redefining event stream of " + streamDefinitionString[0] + ": " + streamDefinitionString[1], e);
                            } catch (RuntimeException e) {
                                log.error("Error in defining event stream " + streamDefinitionString[0] + ": " + streamDefinitionString[1], e);
                            } catch (StreamDefinitionStoreException e) {
                                log.error("Error in defining event stream in store " + streamDefinitionString[0] + ": " + streamDefinitionString[1], e);
                            } finally {
                            	PrivilegedBarleyContext.endTenantFlow();
                            }
                        } catch (MalformedStreamDefinitionException e) {
                            log.error("Malformed Stream Definition for " + streamDefinitionString[0] + ": " + streamDefinitionString[1], e);
                        } catch (UserStoreException e) {
                            log.error("Error in identifying tenant event stream " + streamDefinitionString[0] + ": " + streamDefinitionString[1], e);
                        }
                    }
                } catch (Throwable t) {
                    log.error("Cannot load stream definitions ", t);
                }

                /* (임시주석)
                receiverServiceRegistration = context.getBundleContext().
                        registerService(DataBridgeReceiverService.class.getName(), databridge, null);
                subscriberServiceRegistration = context.getBundleContext().
                        registerService(DataBridgeSubscriberService.class.getName(), databridge, null);
//                databridgeRegistration =
//                        context.getBundleContext().registerService(DataBridge.class.getName(), databridge, null);
                */
                log.info("Successfully deployed Agent Server ");
            }
        }  catch (RuntimeException e) {
            log.error("Error in starting Agent Server ", e);
        }
    }


    public void deactivate() {
//        context.getBundleContext().ungetService(receiverServiceRegistration.getReference());
//        context.getBundleContext().ungetService(subscriberServiceRegistration.getReference());
//        databridgeRegistration.unregister();
        if (log.isDebugEnabled()) {
            log.debug("Successfully stopped agent server");
        }
    }

    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public void unsetAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = null;
    }

    protected void setRealmService(RealmService realmService) {
        DataBridgeServiceValueHolder.setRealmService(realmService);
    }

    protected void unsetRealmService(RealmService realmService) {
        DataBridgeServiceValueHolder.setRealmService(null);
    }

    public void setEventStreamStoreService(
            AbstractStreamDefinitionStore abstractStreamDefinitionStore) {
        DataBridgeServiceValueHolder.setStreamDefinitionStore(abstractStreamDefinitionStore);
    }

    public void unsetEventStreamStoreService(
            AbstractStreamDefinitionStore abstractStreamDefinitionStore) {
        DataBridgeServiceValueHolder.setStreamDefinitionStore(null);
    }

    protected void setConfigurationContextService(ConfigurationContextService contextService) {
        DataBridgeServiceValueHolder.setConfigurationContextService(contextService);
    }

    protected void unsetConfigurationContextService(ConfigurationContextService contextService) {
        DataBridgeServiceValueHolder.setConfigurationContextService(null);
    }
    
    // Holder가 존재하지 않아 직접 DS에서 생성된 DataBridge를 리턴함.
    public DataBridgeReceiverService getDataBridgeReceiverService() {
    	return this.databridge;
    }
    public DataBridgeSubscriberService getDataBridgeSubscriberService() {
    	return this.databridge;
    }

}
